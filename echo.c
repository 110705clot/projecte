/***************************************************************************
 *   Copyright (C) 2013 by echo                                            *
 *   daniel@daw2-m08-gui                                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h> 
#include <string.h>//error 1

#define NUM_PORT		13800
#define LONG_MAX_CAD		2048
#define LONG_MAX_NOM_HOST	20

int main(int Count, char *Strings[])
{
	int descriptor_de_socket_servidor, descriptor_socket_client;
	struct sockaddr_in adressa_servidor, adressa_client;
	int longitud_adressa_client=sizeof(adressa_client);
	char cadena_carecters[LONG_MAX_CAD], nom_host[LONG_MAX_NOM_HOST];//error3
	size_t long_cadena_caracters_rebuda;

	/*---Creació d'un socket TCP---*/
	if ( (descriptor_de_socket_servidor = socket(AF_INET, SOCK_STREAM, 0)) < 0 );
	{
		perror("Socket");
		exit(errno);//error2
	};

	/*---Assignació d'adreça IP i número de port---*/
	bzero(&adressa_servidor, sizeof(adressa_servidor));
	adressa_servidor.sin_family = AF_INET;
	adressa_servidor.sin_port = htons(NUM_PORT);
	adressa_servidor.sin_addr.s_addr = INADDR_ANY;
	if ( bind(descriptor_de_socket_servidor, (struct sockaddr*)&adressa_servidor, sizeof(adressa_servidor)) != 0 )
	{
		perror("socket--bind");
		exit(errno);
	}

	/*---Preparem  Ara fem que el servidor coemcni a escoltar---*/
	if ( listen(descriptor_de_socket_servidor, 20) != 0 )
	{
		perror("socket--listen");
		exit(errno);
	}

	/*---Aquest llaç mai no filnalitza. El servidor estarà permanentment escolt_servidorant i responen ---*/
	/*---Hauriem de fer un kill per aturar el servidor---*/
	while (1)
	{	/*---S'accepta una connexió---*/
		descriptor_socket_client = accept(descriptor_de_socket_servidor, (struct sockaddr*)&adressa_client, &longitud_adressa_client);
		printf("%s:%d connected\n", inet_ntoa(adressa_client.sin_addr), ntohs(adressa_client.sin_port));
		/*---Fem un Echo cap el client de qualsvol cosa que ens faci arribar---*/
		long_cadena_caracters_rebuda=recv(descriptor_socket_client, cadena_carecters, LONG_MAX_CAD, 0);
		send(descriptor_socket_client, cadena_carecters, long_cadena_caracters_rebuda, 0);
		/*---Tanquem la connexió amb el client---*/
		close(descriptor_socket_client);
	}

	/*---Tanquem el socket obert i finalitzem el programa. Teòricament, aquí no podem arribar---*/
	clos(descriptor_de_socket_servidor);
	return 0;
}

